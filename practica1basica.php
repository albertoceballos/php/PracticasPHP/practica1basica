<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
    </head>
    <body>
        <?php
            function color(){
                $r=rand(0,255);
                $g=rand(0,255);
                $b=rand(0,255);
                $color="rgb($r,$g,$b)";
                return $color;
            }
            
            function dibujarCirculo($x,$y){
                echo '<circle cx="'.$x.'" cy="'.$y.'" r="50" fill="'. color().'"/>';
                
            }
            
            $longitud= rand(10, 1000);
            echo "longitud: $longitud";
            echo '<br>';
        ?>
        <svg width="<?= $longitud ?>px" height="10px"> 
        <?php
        echo '<line x1="1" y1="5" x2="'.$longitud.'" y2="5" stroke="black" stroke-width="10">';
        ?>
        </svg>
        <br><br>Circulo con HTML y función en PHP para color aleatorio:
        <div class="circulo" style="width:50px;height:50px;border-radius:50%;background-color:<?= color() ?>"></div>
        
        Color aleatorio mediante PHP:
        <svg>
         <circle cx="50" cy="50" r="50" fill="<?= color() ?>"/>
        </svg>
        Dibuja circulos pasando X,Y en funcion PHP:
        <svg width="500px" height="500px">
         <?php
            dibujarCirculo(200, 150);
            dibujarCirculo(250, 350);
            dibujarCirculo(200, 450);
         ?>
        </svg>
        
        
       
        <script type="text/javascript">
           /* document.querySelector("line").addEventListener("click",salta);
            
            function salta(event){
                var valor;
                valor=Math.random()*(100-1)+1;
                event.target.setAttribute("x2",valor);
            }*/
            
            document.querySelector("svg").addEventListener("mouseover",function(){
                document.body.style.cursor="pointer";
            });
            
            document.querySelector("svg").addEventListener("click",function(){
                  location.reload();
            });
            
            
            //Hacer que  cada vez que se apriete un círculo se oculta este
            //para los círculos dibujados con SVG:
            var circulos=document.querySelectorAll("circle");
            for(i=0;i<circulos.length;i++){
               circulos[i].addEventListener("click",ocultaCirculo);
            }
            //para el círculo que es un div:
            document.querySelector(".circulo").addEventListener("click",ocultaCirculo);
            
            function ocultaCirculo(event){
                event.target.style="display:none";
            }
            
        </script>
    </body>
    
</html>




